#include <cstdio>

const int k = 31543;

unsigned pow(unsigned a, unsigned b) {
    // printf("%d %d\n", a, b);
    if (b == 0) return 1;
    else if (b == 1) return a;
    else if (b == 2) return a*a%k;

    unsigned r = pow(a%k, b/2)%k;
    r = (r*r)%k;
    if (b%2) { // impar
        return (a*r)%k;
    } else {
        return r;
    }
}

int main() {
    unsigned a, b;
    scanf("%d %d\n", &a, &b);
    while(a || b) {
        printf("%d\n", pow(a%k, b));
        scanf("%d %d\n", &a, &b);
    }

    return 0;
}