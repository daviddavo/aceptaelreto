#include <iostream>
#include <math.h>

void printInt(int n) {
    if (n/10) printInt(n/10);
    putchar_unlocked(n%10+'0');
}

bool readInt(int *n) {
    *n = 0;
    register char c = getchar_unlocked();
    for (; c<'0' || c>'9'; c = getchar_unlocked()) {
        // printInt(c); putchar_unlocked('\n');
        if (c == EOF) return false;
    }

    for (; c>='0' && c<='9'; c = getchar_unlocked()) { 
        // printInt(c); putchar_unlocked('\n');
        if (c == EOF) return false;
        *n = *n*10+c-'0';
    }

    return true;
}

// Lo más obvio es meter 1
int main() {
    int n;
    while (readInt(&n)) {
        printInt(log2(n)+1);
        putchar_unlocked('\n');
    }

    return 0;
}