#include <cstdio>

bool readInt(int *n) {
    *n = 0;
    register char c = getchar_unlocked();
    for (; c<'0' || c>'9'; c = getchar_unlocked()) {
        // printInt(c); putchar_unlocked('\n');
        if (c == EOF) return false;
    }

    for (; c>='0' && c<='9'; c = getchar_unlocked()) { 
        // printInt(c); putchar_unlocked('\n');
        if (c == EOF) return false;
        *n = *n*10+c-'0';
    }

    return true;
}

bool solve(int ini, int fin, int n, int k) {
    // COMO HAY RECURSION FINAL PODEMOS IMPLEMENTARLO ITERATIVO
    for (int i = 0; i < k; i++) {
        int l;
        readInt(&l);

        if (l <= n && n > ini) ini = l;
        else if (l > n && l < fin) fin = l;
    }

    // printf("ini: %d, fin: %d, n: %d, k: %d\n", ini, fin, n, k);
    /*
    if (k > 0) {
        int l;
        scanf("%d", &l);

        if (fin-ini <= 1) return solve(ini, fin, n, k-1) || true; // We have to finish reading

        if (l <= n && l > ini) {
            // printf("  l:\t%d <= %d\n", l, n);
            return solve(l, fin, n, k-1);
        } else if (l > n && l < fin) {
            // printf("  l:\t%d > %d\n", l, n);
            return solve(ini, l, n, k-1);
        } else {
            return solve(ini, fin, n, k-1);
        }
    } else {
        return fin - ini <= 1;
    }*/
    return fin - ini <= 1;
}

int main() {
    int ini, fin, n, k;

    readInt(&ini);
    readInt(&fin);
    readInt(&n);
    while (ini | fin | n) {
        readInt(&k);

        if (solve(ini, fin+1, n, k)) {
            putchar_unlocked('L');
            putchar_unlocked('O');
            putchar_unlocked(' ');
            putchar_unlocked('S');
            putchar_unlocked('A');
            putchar_unlocked('B');
            putchar_unlocked('E');
            putchar_unlocked('\n');
        } else {
            putchar_unlocked('N');
            putchar_unlocked('O');
            putchar_unlocked(' ');
            putchar_unlocked('L');
            putchar_unlocked('O');
            putchar_unlocked(' ');
            putchar_unlocked('S');
            putchar_unlocked('A');
            putchar_unlocked('B');
            putchar_unlocked('E');
            putchar_unlocked('\n');
        }

        readInt(&ini);
        readInt(&fin);
        readInt(&n);
    }

    return 0;
}