#!/bin/bash
sum=$(ls -l 402_*.dat | wc -l)
paste -d"\t" 402_*.dat | nawk -v s="$sum" '{
    t1 = $3
    t2 = $5
    t3 = $7

    for ( i = 1; i<s;i++) {
        if ($(3+i*7) > 0 && $(3+i*7) < t1 )
            t1 = $(3+i*7)

        if ($(5+i*7) > 0 && $(5+i*7) < t2 )
            t2 = $(5+i*7)

        if ($(7+i*7) > 0 && $(7+i*7) < t3 )
            t3 = $(7+i*7)

    }

    print $1"\t"$2"\t"t1"\t"$4"\t"t2"\t"$6"\t"t3"\t"c
}'
