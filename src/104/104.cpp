#include <cstdio>

bool solve(int, int, int, int, int*);

bool solve(int * pesoTotal) {
    int p1, p2, d1, d2;
    scanf("%d %d %d %d\n", &p1, &d1, &p2, &d2);
    return solve(p1, d1, p2, d2, pesoTotal);
}

bool solve(int p1, int d1, int p2, int d2, int * pesoTotal) {
    bool ret1 = true;
    if (p1 == 0) {
        ret1 = solve(pesoTotal);
        p1 = *pesoTotal;
    }
    if (p2 == 0) {
        ret1 = solve(pesoTotal) && ret1;
        p2 = *pesoTotal;
    }

    *pesoTotal = p1 + p2;
    // printf("%d*%d-%d*%d = %d, p: %d\n", p1, d1, p2, d2, p1*d1-p2*d2, *pesoTotal);
    return ret1 && p1*d1 == p2*d2;
}

bool solve(int p1, int d1, int p2, int d2) {
    int pesoTotal;
    return solve(p1, d1, p2, d2, &pesoTotal);
}

int main() {
    int p1, d1, p2, d2;

    scanf("%d %d %d %d\n", &p1, &d1, &p2, &d2);
    while(p1+d1+p2+d2) {
        printf(solve(p1,d1,p2,d2)?"SI\n":"NO\n");

        scanf("%d %d %d %d\n", &p1, &d1, &p2, &d2);
    }

    return 0;
}