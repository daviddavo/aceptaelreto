#include <iostream>

using namespace std;

// Pre: {0<n,m<10^8}
long long int solve(long long int n, long long int m) {
    return (n+m)<<1;
}
// Pos: r = {n*2+m*2}

int main() {
    long long int n, m;
    cin >> n >> m;
    while (n >= 0 && m >= 0) {
        cout << solve(n, m) << "\n";

        cin >> n >> m;
    }

    return 0;
}