#include <signal.h>
#include <time.h>
#include <math.h>
#include <stdio.h>
#include <dirent.h>
#include <sys/stat.h>
#include <cuda.h>
#include <curand.h>

#define MAX_SIZE 100000
// MAX_SIZE Should be something that MAX_SIZE = (THREADS (32) * n)(32*n+1)/2 where n is an integer
#define MUESTRAS 1000
const clockid_t DEFAULT_CLOCK = CLOCK_THREAD_CPUTIME_ID;
const curandRngType_t DEFAULT_GENERATOR = CURAND_RNG_PSEUDO_DEFAULT;

__device__
__host__
int solveFast(const bool v[], int n, int m) {
    int vi = 0, vf = 0;
    int ret = 0;
    int maxw = 0;
    int nCons = 0;

    while (vf < n) {
        if(v[vi] == 0) {
            vi++;
        } else {
            if (v[vf]) {
                maxw = vf-vi+1;
                nCons = 0;
            } else {
                nCons++;
            }

            vf++;
        }

        if (nCons > m) {
            nCons--;
            vi++;
            vf = vi;
        }

        if (maxw > ret) {
            ret = maxw;
        }

        if (vi > vf) {
            vf = vi;
        }
    }

    return ret;
}

int solveBucles(bool v[], int n, int m) {
    int vi = 0, vf = 0;
    int ret = 0;
    int maxw = 0;
    int nCons = 0;

    while (vf < n) {
        if (v[vi] == 0) {
            while (!v[vi]) {
                vi++;
            }
        }

        else {
            if (v[vf] == 1) {
                while (v[vf] && vf < n) {
                    maxw = vf - vi + 1;
                    vf++;
                    nCons = 0;
                }
            }
            else {
                nCons++;
                vf++;
            }

        }

        if (nCons > m) {
            nCons--;
            vi++;
            vf = vi;
        }

        if (maxw > ret)
            ret = maxw;

        if (vi > vf)
            vf = vi;
    }

    return ret;
}

timespec operator-(timespec a2, timespec a1) {
    timespec res;
    if (a2.tv_nsec < a1.tv_nsec) {
        res.tv_sec = a2.tv_sec - a1.tv_sec -1;
        res.tv_nsec = a2.tv_nsec - a1.tv_nsec + 1000000000;
    } else {
        res.tv_sec = a2.tv_sec - a1.tv_sec;
        res.tv_nsec = a2.tv_nsec - a1.tv_nsec;
    }

    return res;
}

void testFor_n(bool v[], int size, int k, FILE * f, float offset = 0) {
    timespec start, stop, stop2;
    for (int j = (int)( (float)k/(float)MUESTRAS*offset ); j < size; j += (size-k+MUESTRAS-1)/MUESTRAS) {
            clock_gettime(DEFAULT_CLOCK, &start);
            solveFast(v,j,k);
            clock_gettime(DEFAULT_CLOCK, &stop);
            solveBucles(v,j,k);
            clock_gettime(DEFAULT_CLOCK, &stop2);
            fprintf(f, "%d %d %d.%09d %d.%09d\n", j, k, 
                (stop-start).tv_sec, (stop-start).tv_nsec,
                (stop2-stop).tv_sec, (stop2-stop).tv_nsec);
            printf("%d %d %d.%09d %d.%09d\n", j, k, 
                (stop-start).tv_sec, (stop-start).tv_nsec,
                (stop2-stop).tv_sec, (stop2-stop).tv_nsec);
    }
}

bool is_file(const char * fname) {
    struct stat buf;
    stat(fname, &buf);
    return S_ISREG(buf.st_mode);
}

char * getNextFname(const char base_fname[], int * fnameNmbr = nullptr) {
    char * strformat = (char*)malloc(sizeof(char) * 40);
    char fname[50];
    strcpy(strformat, "./data/");
    strcat(strformat, base_fname);
    strcat(strformat, "%02d.dat");
    int i = 0;

    sprintf(fname, strformat, i);
    while (i < 256 && is_file(fname)) {
        i++;
        sprintf(fname, strformat, i);
    }

    char * path = (char*)malloc(sizeof(char)*40);
    sprintf(path, "./data/%s%02d.dat", base_fname, i); // Kids don't do this at home
    if (fnameNmbr != nullptr) {
        *fnameNmbr = i;
    }
    return path;
}

__global__
void threadGenerateWorst(bool v[], int k, int size) {
    int tid = threadIdx.x + blockIdx.x*blockDim.x;
    // Starting position in the array
    int spa = tid*(2*k+tid+1)/2;
    for (int i = 0; i < k; i++) v[spa+i] = 0;
}

__host__
void generateWorstArray(bool arr[], int k, int size, FILE * f = stdout) {
    // El peor de los casos se dará cuando hay cada vez más 1's entre los ceros
    // Activando siempre los if nCons > m
    bool * aux;
    // To calculate the total number of "threads" that we need, we follow the formula
    // size+1=n*k+(n+1)*n/2, where n should be the number of total threads
    // WTF int totalThreads = (-2*k-1 + sqrt( (2*k+1)*(2*k+1)+8*(size+11) ) )/2;
    int totalThreads = (-2*k-1 + sqrt( (1+2*k)*(1+2*k)+8*size) +2-1 )/2;
    int threads = 2048;
    int blocks = (totalThreads + threads -1)/threads;
    // int blocks = (totalThreads%threads != 0)?(totalThreads/threads+1):(totalThreads/threads);
    fprintf(f, "# Total threads: %d\n", totalThreads);
    fprintf(f, "# Blocks: %d\n", blocks);
    int cudaSize = totalThreads*k+(totalThreads+1)*(totalThreads)/2+k+1;
    fprintf(f, "# Size: %d, cudaSize: %d\n", size, cudaSize);
    cudaMalloc(&aux, cudaSize*sizeof(bool));
    fprintf(f, "# Cuda: %d\n", cudaMemsetAsync(aux, 1, cudaSize*sizeof(bool)));
    cudaDeviceSynchronize();
    threadGenerateWorst<<<blocks, threads>>>(aux, k, cudaSize);
    cudaDeviceSynchronize();
    fprintf(f, "# Cuda: %d\n", cudaMemcpy(arr, aux, size*sizeof(bool), cudaMemcpyDeviceToHost));
    cudaFree(aux);
}

void generateWorstCPU(bool v[], int k, int size, FILE * f = stdout) {
    memset(v, 1, MAX_SIZE * sizeof(bool));

    int totalThreads = (-2*k-1 + sqrt( (1+2*k)*(1+2*k)+8*size) +2-1 )/2;

    for (int i = 0; i < totalThreads; i++) {
        int spa = i*(2*k+i+1)/2;
        for (int j = 0; j < k; j++) if (spa+j < size) v[spa+j] = 0;
    }
}

__global__
void unsignedToBool(unsigned f[], bool b[]) {
    // 0.0 is excluded, 1.0 included (Cuda Toolkit Documentation)
    int tid = threadIdx.x + blockIdx.x*blockDim.x;
    b[tid] = f[tid] & 0b10;
}

__host__
void generateRandomBoolArray(bool arr [], int size, unsigned long long seed) {

    curandGenerator_t gen;
    printf("cuRand: %d\n", curandCreateGenerator(&gen, DEFAULT_GENERATOR));
    printf("cuRand: %d\n", curandSetPseudoRandomGeneratorSeed(gen, seed));
    // curandGenerateUniform(rngen, randarr, size);

    unsigned * randarr;
    bool * aux;
    printf("cuda: %d\n", cudaMalloc(&aux, size*sizeof(bool)));
    printf("cuda: %d\n", cudaMalloc(&randarr, size*sizeof(int)));

    printf("cuRand: %d\n", curandGenerate(gen, randarr, size));

    // Array of <size> floats generated. Now convert to bool concurrently
    int threads = 2048;
    int blocks = (size%threads != 0)?(size/threads+1): (size/threads);
    cudaDeviceSynchronize();
    unsignedToBool<<<blocks, threads>>>(randarr, aux);
    printf("Cuda: %d\n", cudaDeviceSynchronize());


    printf("Cuda: %d\n", cudaMemcpy(arr, aux, size*sizeof(bool), cudaMemcpyDeviceToHost));
    printf("Seed: %llu\n", seed);
    printf("CuRand: %d\n", curandDestroyGenerator(gen));
    cudaFree(aux);
    cudaFree(randarr);
}

void testAll(bool * v, int size, FILE * fmain, float offset = 0) {
    timespec total_start, total_stop;
    clock_gettime(DEFAULT_CLOCK, &total_start);

    fprintf(fmain, "# Offset: %f\n", offset);
    fprintf(fmain, "# Starting i: %d\n", (int)((float)(size)*offset/(float)(MUESTRAS)));
    fprintf(fmain, "# Increments: %d\n", size/MUESTRAS);
    for (int i = (int)((float)(size)*offset/(float)(MUESTRAS)); i < size; i+=size/MUESTRAS) {
        testFor_n(v, MAX_SIZE, i, fmain, offset);
    }

    clock_gettime(DEFAULT_CLOCK, &total_stop);
    printf("Total CPU: %d.%09d\n", (total_stop-total_start).tv_sec, (total_stop-total_start).tv_nsec);
    fprintf(fmain, "# Total CPU: %d.%09d\n", (total_stop-total_start).tv_sec, (total_stop-total_start).tv_nsec);
}

void allSame(bool k) {
    bool * v = (bool*)malloc(sizeof(bool)*MAX_SIZE);

    int fnameNmbr;
    char * fname = getNextFname(k?"all1s":"all0s", &fnameNmbr);
    FILE * f = fopen(fname, "w+");

    fprintf(f, "# all%ds", k);
    memset(v, k, MAX_SIZE * sizeof(bool));
    testAll(v, MAX_SIZE, f, (float)fnameNmbr/10.0);

    free(v);
    fclose(f);
}

int printArr(bool v[], int size, FILE * f) {
    int trues = 0;
    for (int i = 0; i < size; i++) {
        fputc(v[i]?'1':'0', f);
        trues += v[i];
    }
    fputc('\n', f);

    return trues;
}


void promCaso() {
    bool * v = (bool*)malloc(MAX_SIZE * sizeof(bool));
    generateRandomBoolArray(v, MAX_SIZE, clock());

    int fnameNmbr;
    char * fname = getNextFname("average", &fnameNmbr);
    printf("Saving to file: %s\n", fname);
    char fnameArr[40];
    sprintf(fnameArr, "./data/averageSourceArray%02d.bin", fnameNmbr);
    
    // SAVE SOURCE DATA TO FILE
    // As sizeof(char) = sizeof(bool), we'll write it as char, so we can "cat" it
    FILE * farr = fopen(fnameArr, "wb+");
    int trues = printArr(v, MAX_SIZE, farr);
    printf("Trues: %d, Pct. trues: %f\n", trues, (float)trues/(float)MAX_SIZE*100);
    fclose(farr);

    FILE * f = fopen(fname, "w+");
    testAll(v, MAX_SIZE, f, (float)fnameNmbr/(float)10);
    // testAll(v, MAX_SIZE, f, farr, 0.5);
    fclose(f);

    free(v);
}

void peorCaso() {
    bool * v = (bool*)malloc(sizeof(bool)*MAX_SIZE);

    int fnameNmbr;
    char * fname = getNextFname("worst", &fnameNmbr);
    printf("Saving to file: %s\n", fname);
    char fnameArr[40];
    sprintf(fnameArr, "./data/worstArray%02d.bin", fnameNmbr);
    FILE * f = fopen(fname, "w+");
    // testAll(v, MAX_SIZE, f, farr, (float)fnameNmbr/10.0);

    timespec total_start, total_stop;
    clock_gettime(DEFAULT_CLOCK, &total_start);

    float offset = (float)fnameNmbr/10.0;
    int size = MAX_SIZE;
    fprintf(f, "# worst array over k+1");
    fprintf(f, "# Offset: %f\n", offset);
    fprintf(f, "# Starting i: %d\n", (int)((float)(size)*offset/(float)(MUESTRAS)));
    fprintf(f, "# Increments: %d\n", size/MUESTRAS);
    for (int i = (int)((float)(size)*offset/(float)(MUESTRAS)); i < size; i+=size/MUESTRAS) {
        generateWorstArray(v, i+1, MAX_SIZE, f);
        testFor_n(v, size, i, f, offset);
    }

    clock_gettime(DEFAULT_CLOCK, &total_stop);
    printf("Total CPU: %d.%d\n", (total_stop-total_start).tv_sec, (total_stop-total_start).tv_nsec);
    fprintf(f, "# Total CPU: %d.%d\n", (total_stop-total_start).tv_sec, (total_stop-total_start).tv_nsec);

    fclose(f);
    free(v);
}

void testSpeed() {
    timespec start_proc, stop_proc, start_cuda, stop_cuda;
    clock_gettime(DEFAULT_CLOCK, &start_proc);
    bool * v = new bool [MAX_SIZE];
    for (int i = 0; i < MAX_SIZE; i += MAX_SIZE/MUESTRAS) {
        generateWorstCPU(v, i, MAX_SIZE);
    }
    clock_gettime(DEFAULT_CLOCK, &stop_proc);
    for (int i = 0; i < MAX_SIZE; i += MAX_SIZE/MUESTRAS) {
        generateWorstArray(v, i, MAX_SIZE);
    }
    clock_gettime(DEFAULT_CLOCK, &stop_cuda);
    printf("Total CPU: %d.%09d\n", (stop_proc-start_proc).tv_sec, (stop_proc-start_proc).tv_nsec);
    printf("Total GPU: %d.%09d\n", (stop_cuda-stop_proc).tv_sec, (stop_cuda-stop_proc).tv_nsec);
}

int main(int argc, char * argv[]) {

    // signal (SIGINT, handler);

    if (argc < 2) {
        printf("Not enough arguments\n");
        printf("0 - All 0s\n1 - All 1s\n2 - Random array\n3 - worst Array\n");
        return 1;
    } else {
        int n = 1;
        if (argc == 3) {
	  sscanf(argv[2], "%d", &n);    
        }
	printf("Executing %d times\n", n);
        for (int i = 0; i < n; i++) {
            switch(argv[1][0]) {
                case '0':
                    allSame(0);
                    break;
                case '1':
                    allSame(1);
                    break;
                case '2':
                    promCaso();
                    break;
                case '3':
                    peorCaso();
                    break;
                case '5':
                    testSpeed();
                    break;
                default:
                    printf("Invalid argument: %s\n", argv[1]);
            }
        }
    }

        /*
    time_t end = time(nullptr) + 6*3600;
    for (int i = 0; i < 9; i++) {
        mejorCaso();
        peorCaso();
    }
    while (time(nullptr) < end) {
        promCaso();
    }*/

    cudaDeviceReset();

    return EXIT_SUCCESS;
}
