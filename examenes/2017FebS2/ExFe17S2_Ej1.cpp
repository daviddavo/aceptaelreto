#include <iostream>

using namespace std;

const int MAX_TAM = 100000;

// Definimos los siguientes predicados que usaremos en la poscondición
// e invariante
/*
    frec(V,n,e):    ( # i : 0 <= i < n : V[i] = e )
    perm(V,W,n): ( p.t. i : 0 <= i < n : frec(V,n,v[i]) = frec(W,n,v[i]) )
*/

// La complejidad del algoritmo en el peor caso es del orden O(n) (lineal), 
// pues ejecutamos el bucle exactamente n veces

// { Pre: 0 <= n < tam(v) ^ V = v}
int solve(int v[] /* out V */, int n) { /* return primerNegativo */
    int i = 0, primerNegativo = 0;

    // Cota: C = n - i
    // Nota: I es una variable libre, no ligada
    // { I: perm(v,V,i) ^
    //      0 <= i <= n ^
    //      (p.t j : 0 <= j < primerNegativo : v[j] >= 0) ^
    //      (p.t j : primerNegativo < j < i  : v[j] <  0)
    // }
    while (i < n) {
        if (v[i] >= 0)
            swap(v[i], v[primerNegativo++]);

        ++i;
    }

    return primerNegativo;
}
// { Pos: perm(v,V,n) ^ 
//        (p.t. i : 0 <= i < primerNegativo : v[i] >= 0) ^
//        (p.t. i : primerNegativo <= i < n : v[i] <  0)}

int main() {
    int v[MAX_TAM];
    int nCasosPrueba, nArray, ret;

    cin >> nCasosPrueba;
    for (int i = 0; i < nCasosPrueba; i++) {
        cin >> nArray;
        for (int j = 0; j < nArray; j++)
            cin >> v[j];

        ret = solve(v, nArray);

        for (int j = 0; j < nArray; j++)
            cout << v[j] << " ";
        cout << "\n" << ret << "\n";
    }

    return 0;
}