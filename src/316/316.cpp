#include <iostream>
using namespace std;

const int MAX_SIZE = 100000;

int solve(int v[], int * n /*inout n=r2 */ ) {
    int sum = 0, maxsum = 0;
    int i = 1, maxi = 1;
    int f = 1, maxf = 1;

    int j = 0;
    while (j < *n) {

        if (sum <= 0) {
            i = f = j+1;
            sum = v[j];
        } else {
            ++f;
            sum += v[j];
        }

        if (sum > maxsum || (sum == maxsum && f - i < maxf - maxi) ) {
            maxi = i;
            maxf = f;
            maxsum = sum;
        }

        ++j;
    }

    *n = maxf;
    return maxi;
}

int main() {
    int n, m, v[MAX_SIZE], r;
    scanf("%d", &n);
    for (int i = 0; i < n; ++i) {
        scanf("%d", &m);
        for (int j = 0; j < m; ++j)
            scanf("%d", &v[j]);

        r = solve(v, &m);
        printf("%d %d\n", r, m);
    }

    return 0;
}