#include <iostream>
using namespace std;

const long long int MOD = 1000000007;
const int MAX_NUM = 5000;

// b < long(v)
void solve(int a, int b, long long int v[]) {
    if (a == 1 || a == b) {
        v[b] = 1;
    } else {
        v[b] = v[b-1] + v[b-1];
    }
}
// Post: pt.i 0<=i<b : v[i] = "a sobre i"

int main() {
    int a, b;
    long long int v[MAX_NUM];
    scanf("%d %d\n", &a, &b);
    while (a || b) {
        solve(a, b, v);

        scanf("%d %d\n", &a, &b);
    }

    return 0;
}