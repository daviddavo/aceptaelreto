#include <cstdio>

bool solve(char ovejas[50][50], int alto, int ancho) {
    bool ret = false;

    for (int i = 0; i < alto; i++) {
        for (int j = 0; j < ancho; j++) {
            ovejas[i][j]
        }
    }
}

bool readInt(char *n) {
    *n = 0;
    register char c = getchar_unlocked();
    for (; c<'0' || c>'9'; c = getchar_unlocked()) {
        // printInt(c); putchar_unlocked('\n');
        if (c == EOF) return false;
    }

    for (; c>='0' && c<='9'; c = getchar_unlocked()) { 
        // printInt(c); putchar_unlocked('\n');
        if (c == EOF) return false;
        *n = *n*10+c-'0';
    }

    return true;
}

int main() {
    char alto, ancho;
    char ovejas[50][50];
    while(readInt(&ancho) && readInt(&alto)) {
        for (int i = 0; i < alto; i++)
            for (int j = 0; j < ancho; j++)
                ovejas[i][j] = getchar_unlocked();

        if (solve(ovejas, ancho, alto)) {
            putchar_unlocked('S');
            putchar_unlocked('I');
            putchar_unlocked('\n');
            continue;
        }
        putchar_unlocked('N');
        putchar_unlocked('O');
        putchar_unlocked('\n');
    }

    return 0;
}