#include <iostream>
using namespace std;

const int NUM_MAX = 3;

// { Pre: 0 <= pos <= N <= tamaño(arr) }
void insertar(int v [] /*inout*/, int N, int elemento, int pos) {
    // { I :
    // ( pos <= i <= N-1 ) ^
    // ( ∀ i : pos <= i < N-1 : V[i+1] = v[i] )
    // }
    int i = N-1;
    while (i > pos) {
        v[i] = v[i-1];
        i--;
    }

    v[pos] = elemento;
}
// { Pos: ( V[pos] = elemento ) ^ ( ∀ i : pos<i<N-1 : V[i+1]=v[i] ) }

/*
El algoritmo consta de dos bucles:
- Uno para calcular los máximos, que tiene
    en su interior otro bucle que depende de k,
    por lo que tendrá una complejidad O(k*n) (lineal), en el caso peor.
- Otro para sumar estos máximos, de complejidad lineal [O(n)]
- Si sumamos ambos, tenemos O(n) + O(k^2*n) = O(n*k^2) = O(n), pues sólo
    queremos la complejidad respecto al tamaño de los datos, n.
    Establecemos que k será igual que 3 para este problema
*/

/*
Nota para el corrector:
Sé que la especificación del algoritmo no es correcto, fallaría en la 
postcondición y en el primer invariante cuando hacemos (maxs[j] = max i : 0<=i<n : v[i] )
Si fuese así, al hacer el sumatorio haríamos ret = k * (max j : 0<=j<n : v[j])

Para terminar de resolver el ejercicio falta:
1. corregir la postcondición y el invariante
2. quitar el break y sustituir el && (corrección)
*/
// Pre: { ( k <= n <= longitud(v) ) ^ ( ∀ i : 0<=i<n : v[i] >= 0 ) }
int solve(int v[], int n, int k = NUM_MAX) {
    int * maxs = new int [k] { 0 };
    int i = 0;

    // { I : 
    // ( 0<=i<=n ) ^
    // ( ∀ l : 0<=l<k : (maxs[l] = max j : 0<=j<i : v[i] )) ^
    // ( ∀ l : 0<=l<k-1 : maxs[l] >= maxs[l+1] ) ^
    // }
    while (i < n) {

        // Nota: Se puede poner el invariante como
        // Si existe v[i] >= maxs[j] entonces insertar... ?

        // { I: 
        // ( 0<=j<=k ) ^
        // ( ∀ l : 0<=l<k-1 : maxs[l] >= maxs[l+1] ) ^
        // 
        // }
        int j = 0;
        while ( j < k /* && (correción) */ ) {
            if ( v[i] >= maxs[j] ) {
                insertar(maxs, k, v[i], j);
                break;
            }
            j++;
        }

        i++;
    }

    // { I: 1<=i<=k ^
    //      ret = ∑ j : 0<=j<i : maxs[j]
    // }
    int ret = maxs[0];
    i = 1;
    while (i < k) {
        ret += maxs[i];
        i++;
    }

    return ret;
}
// Post: { 
// ( ∀ j : 0<=j<k : (maxs[j] = max i : 0<=i<n : v[i] )) ^
// ( ∀ j : 0<=j<k-1 : maxs[j] >= maxs[j+1] ) ^
// max i : 0<=i<n : (sum j : 0<=j<k : )
// }

// Update 2018/10/02: Tal vez ret = max i : 0<=i<=n : ( sum j : 0<=j<k : ??? ) )

int main() {

    int m;
    cin >> m;
    while (m) {
        int * v = new int [m];
        for (int i = 0; i < m; i++) cin >> v[i];
        cout << solve(v, m) << "\n";

        cin >> m;
    }

    return 0;
}