#include <iostream>
using namespace std;

const int TAM_MAX = 5000;

// freq(v,e,n): # i : 0<=i<n : v[i] = e
// esDivertido(d,n,v): p.t. i : 0<=i<n : freq(v,v[i],n) = d
// esCrecientePorLosPelos(v,n): p.t. i : 0<=i<n-1 : v[i] = v[i+1] || v[i]+1 = v[i+1]

// { Pre: 0 <= n < tam(v) ^ 0 < d }
bool solve(int d, int n, int v[]) { /* return ret */
    int i = 1;
    int cnt = 1;
    bool ret = true;

    // Cota: C = N-i-1
    // { Inv: 
    //      0<=i<n ^
    //      cnt = freq(v,v[i],i) ^
    //      esDivertido(d,i,v) ^
    //      esCrecientePorLosPelos(v,i)
    // }
    while (i < n && (v[i-1] == v[i] || v[i-1] == v[i]-1) && cnt <= d) {
        if (v[i] != v[i-1])
            cnt = 0;
        ++cnt;
        ++i;
    }

    return i == n && cnt <= d;
}
// { Pos: ret = esDivertido(d, n, v) ^ esCrecientePorLosPelos(v, n) }

int main() {
    int casosDePrueba, d, n;
    int v[TAM_MAX];
    scanf("%d", &casosDePrueba);
    for (int i = 0; i < casosDePrueba; i++) {
        scanf("%d %d", &d, &n);
        for (int j = 0; j < n; j++)
            scanf("%d", &v[j]);

        printf("%s\n", solve(d, n, v)?"SI":"NO");
    }

    return 0;
}