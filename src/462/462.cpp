// La verdad es que este problema es bastante interesante
// y me ha llegado la historia al corazón
// Es más, me acabo de limpiar las gafas antes de mirar a la pantalla de nuevo
#include <iostream>

using namespace std;

void solve(unsigned int& g, unsigned int& h, unsigned int& m, unsigned int& s) {
	// Es necesario long long int pues 100000 * (23*3600+59*60+59) > 2^32
	unsigned long long int total = g * (h * 3600 + m * 60 + s);
	s = total % 60;
	m = total / 60 % 60;
	h = total / 3600 % 24;
	g = total / (3600*24);
}

int main () {
	unsigned int n, g, h, m, s;
	char basura;
	cin >> n;
	for (unsigned int i = 0; i < n; i++) {
		cin >> g; // Numero de veces que se limpia las gafas en un día
		cin >> h >> basura >> m >> basura >> s;
		solve(g, h, m, s);
		printf("%d %02d:%02d:%02d\n", g, h, m, s);
	}

	return 0;
}