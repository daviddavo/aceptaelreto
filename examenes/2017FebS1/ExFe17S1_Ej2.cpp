#include <iostream>
using namespace std;

const int MAX_NUM = 50000;

// { Pre: creciente(v,a,b) }
bool solve(int v[], int a, int b) {
    if (b-a < v[b] - v[a])
        return false;

    if ( b-a < 1 ) {
        return v[b]-v[a]<=1;
    }

    int m = (a+b)/2;

    if (v[m] != v[m+1] && v[m]+1 != v[m+1])
        return false;

    return solve(v, a, m) && solve(v, m+1, b);
}
// { Pos: ret = crecientePorLosPelos(v,a,b) }

bool solve(int v[], int n) {
    return solve(v, 0, n-1);
}

int main() {
    int cdp, n;
    int v[MAX_NUM];

    scanf("%d", &cdp);
    for (int i = 0; i < cdp; i++) {
        scanf("%d", &n);
        for (int j = 0; j < n; j++)
            scanf("%d", &v[j]);

        printf("%s\n", solve(v, n)?"SI":"NO");
    }

    return 0;
}