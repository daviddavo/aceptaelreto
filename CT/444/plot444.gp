#!/usr/bin/gnuplot

xmax = 100000
ymax = 0.002

# 1: worst style
set style line 1 pt 5 lc rgb "#e51e10"
set style line 2 pt 5 lc rgb "#009e73"
set style line 3 pt 5 lc rgb "#e69f00"
set style line 7 pt 5 lc rgb "#56b4e9"

set xlabel "n"
set ylabel "tiempo (s)"

a = 0.000175891
b = 2.32232e-12
c = 3.39869e-07
d = 2.20571e-09
f(x) = a+b*x
g(x) = c+d*x
#fit f(x) "data/all0s00.dat" u 1:4 via a,b
#fit g(x) "data/all1s00.dat" u 1:4 via c,d

set macro 
plotBuclesBestn = 'plot [0:xmax] [0:ymax] "data/all0s00.dat" u 1:4 w dots ls 1 notitle, \
    1/0 ls 1 lw 10 w p t "Todo 0s", \
    "data/all1s00.dat" u 1:4 w dots ls 7 notitle, \
    1/0 ls 7 lw 10 w p t "Todo 1s", \
    f(x) lc rgb "#e69f00" lw 3 notitle, \
    g(x) lw 3 notitle, \
    "dots" w p pt 7 lw 10 lc rgb "dark-violet" t "Corte"'

plotBuclesBestk = 'plot [0:xmax] [0:ymax] "data/all0s00.dat" u 2:4 w dots ls 1 notitle, \
    1/0 ls 1 lw 10 w p t "Todo 0s", \
    "data/all1s00.dat" u 2:4 w dots ls 7 notitle, \
    1/0 ls 7 lw 10 w p t "Todo 1s", \
    f(x) lc rgb "#e69f00" lw 3 notitle, \
    g(x) lw 3 notitle, \
    "dots" w p pt 7 lw 10 lc rgb "dark-violet" t "Corte"'    

# set output "complexSolven.png"
set macro
plotComplexSolven = 'plot [0:xmax] [0:ymax] "data/worst00.dat" u 1:3 w dots ls 1 notitle, \
    1/0 ls 1 lw 10 w p t "Caso peor", \
    "data/average00.dat" u 1:3 w dots notitle ls 2, \
    1/0 ls 2 lw 10 w p t "Caso promedio", \
    "data/all0s00.dat" u 1:3 w dots notitle ls 3, \
    1/0 ls 3 lw 10 w p t "Caso mejor"'

set macro
plotComplexSolvek = 'plot [0:xmax] [0:ymax] \
     "data/worst00.dat" u 2:3 w dots ls 1 notitle, \
     1/0 ls 1 lw 10 w p t "Caso peor", \
     "data/average00.dat" u 2:3 w dots notitle ls 2, \
     1/0 ls 2 lw 10 w p t "Caso promedio", \
     "data/all0s00.dat" u 2:3 w dots notitle ls 3, \
     1/0 ls 3 lw 10 w p t "Caso mejor"'

set macro
plotBuclesSolven = 'plot [0:xmax] [0:ymax] "data/worst00.dat" u 1:4 w dots ls 1 notitle, \
    1/0 ls 1 lw 10 w p t "Caso peor", \
    "data/average00.dat" u 1:4 w dots notitle ls 2, \
    1/0 ls 2 lw 10 w p t "Caso promedio"'

set macro
plotBuclesSolvek = 'plot [0:xmax] [0:ymax] "data/worst00.dat" u 2:4 w dots ls 1 notitle, \
    1/0 ls 1 lw 10 w p t "Caso peor", \
    "data/average00.dat" u 2:4 w dots notitle ls 2, \
    1/0 ls 2 lw 10 w p t "Caso promedio"'

# DINA SIZE
set terminal pngcairo enhanced transparent font "Verdana,12" size 1280,1810 fontscale 1.3
ymax = 0.004
set output "graphs/complexSolvek1280x1810.png"
@plotComplexSolvek
set output "graphs/complexSolven1280x1810.png"
@plotComplexSolven
ymax = 0.002
set output "graphs/buclesSolven1280x1810.png"
@plotBuclesSolven
set output "graphs/buclesSolvek1280x1810.png"
@plotBuclesSolvek
set output "graphs/buclesBestn1280x1810.png"
@plotBuclesBestn
set output "graphs/buclesBestk1280x1810.png"
@plotBuclesBestk

# SCREEN SIZE
set terminal pngcairo enhanced transparent font "Verdana,12" size 1280,960 fontscale 1.3
set output "graphs/complexSolven1280x960.png"
@plotComplexSolven
set output "graphs/complexSolvek1280x960.png"
@plotComplexSolvek
set output "graphs/buclesSolven1280x960.png"
@plotBuclesSolven
set output "graphs/buclesSolvek1280x960.png"
@plotBuclesSolvek
set output "graphs/buclesBestn1280x960.png"
@plotBuclesBestn
set output "graphs/buclesBestk1280x960.png"
@plotBuclesBestk

# pause -1 "Press enter to continue"