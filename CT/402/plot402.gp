#!/usr/bin/gnuplot
file = "402best.dat"
set terminal qt

set style lines 1 lw 2 lc "yellow"
set style lines 2 lw 2 lc "red"
set style function lines
set style data dots
#set style function lines 2 lw 2 lc "red"

# # Imagen del algoritmo Accepted
set xlabel "Tamaño del problema"
set ylabel "Tiempo de ejecución (ns)"
xmax=5*10**8
ymax=7*10**4

g(x) = a + b*sqrt(x)
fit [0:xmax] [0:ymax] g(x) file u 1:3 via a,b
m = 2.31
plot [0:xmax] [0:ymax] file u 1:3 t "Caso de prueba", \
    g(x) t "Caso promedio" linestyle 1, \
    1 + m*sqrt(x) t "Caso peor" linestyle 2
pause -1 "Press Enter to continue "

set terminal pngcairo enhanced transparent font "Verdana,12" size 1280,400 fontscale 1.3
set output "AC1280x400.png"
replot
set terminal qt

#Imagen del TLE
m = 2.87	# m stands for magic number
xmax=1/m*10**9
ymax=10**9
f(x) = a+b*x
fit [0:xmax] [0:ymax] f(x) file u 1:5 via a,b

plot [0:xmax] [0:ymax] file u 1:5 t "Caso promedio", \
    f(x) t "Caso promedio" linestyle 1, \
    1 + m*x t "Caso peor" linestyle 2
    # 1 + m*x - 10**9 t "" linestyle 2
pause -1 "Press Enter to continue "

set terminal pngcairo enhanced transparent font "Verdana,12" size 1280,960 fontscale 1.7
set output "TLE1280x960.png"
replot
set terminal qt

# Imagen de sqrt()
xmax=5*10**8
ymax=900

plot [0:xmax] [200:ymax] file u 1:7 t "Caso de prueba"

pause -1 "Press Enter to continue "

set terminal pngcairo enhanced transparent font "Verdana,12" size 1280,400 fontscale 1.3
set output "sqrt1280x400.png"
replot
set terminal qt

# Resultados del algoritmo
set xlabel "Dato 'n'"
set ylabel "Respuesta de la función"

plot file u 1:2 t "Resultados"
pause -1 "Press Enter to continue "

set terminal pngcairo enhanced transparent font "Verdana,12" size 1280,960 fontscale 1.7
set output "results1280x960.png"
replot
set terminal qt

