#include <cstdio>

int solve() {
    char c = getchar();
    switch (c) {
        case '+': return solve() + solve();
        case '-': return solve() - solve();
        case '*': return solve() * solve();
        case '/': return solve() / solve();
        case ' ': return solve();
        // case '\n': return;
        default: return c-'0';
    }
}

int main() {
    int n;
    char * aux;
    scanf("%d\n", &n);
    for (int i = 0; i < n; i++){ 
        printf("%d\n", solve());
        getchar();
    }

    return 0;
}