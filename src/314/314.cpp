#include <iostream>
using namespace std;

// Pre : ( 2 <= n <= longitud (v) )
long long int solve(long long int v[], long long int & n /*inout ret2*/) {
    long long int ret1 = 0, ret2 = 0;
    long long int i = 1;

    // I: { 1 <= i <= n-1 ^
    //      ret1 = # j : 1<=i<j-1 : v[i-1]>v[i] ^ v[i]<v[i+1] ^
    //      ret2 = # k : 1<=i<k-1 : v[i-1]<v[i] ^ v[i]>v[i+1]
    // }
    while ( i < n - 1 ) {
        if ( v[i-1] > v[i] && v[i] < v[i+1] )
            ret1++;
        if ( v[i-1] < v[i] && v[i] > v[i+1] )
            ret2++;

        i++;
    }

    n = ret1;
    return ret2;
}
// Pos: { ret1 = #i : 1 <= i < n-1 : v[i-1]>v[i] ^ v[i]<v[i+1] 
//        ret2 = #j : 1 <= j < n-1 : v[i-1]<v[i] ^ v[i]>v[i+1]
// }

// *-----------
// Anexo: buscar ceros

// {Pre : 0 <= n <= longitud(n) }
/*
int buscaCero(int v[], int n) {
    int i = 0;

    // { I : }
    while (i < n && v[i] != 0) {

        i++;
    }

    return i;
}
*/
// { Pos : }

int main() {
    long long int n, m;
    cin >> n;

    for (long long int i = 0; i < n; i++) {
        cin >> m;
        long long int * v = new long long int [m];
        for (long long int j = 0; j < m; j++) cin >> v[j];
        cout << solve(v, m) << " " << m << "\n";

        delete [] v;
    }

    return 0;
}