#include <iostream>

using namespace std;

int mcd (int a, int b) {
    if(min(a,b) == 0){ return max(a,b); }
    else{ return mcd(max(a,b), max(a,b)%min(a,b)); }
}

// Pre: {0<=n<long(v)}
int solve (int v [], int n) {
    int lcd = v[0], mul = v[0];
    for (int i = 1; i < n; i++) {
        lcd = mcd(lcd, v[i]);
        mul *= v[i];
    }
    // ESTO ESTÁ MAL Y ERES SUBNORMAL
    return mul/lcd;
}

int main () {
    int n, m;
    cin >> n;
    while (n) {
        int * v = new int [n];
        for (int j = 0; j < n; j++) cin >> v[j];
        cout << solve(v, n) << "\n";

        delete [] v;
        cin >> n;
    }

    return 0;
}