#include <iostream>
#include <string>

using namespace std;

/* Puede haber hasta 10^9 folios, por lo que no podremos recibir la entrada en un array
Tendremos que procesarla 'mientras llega' */
bool resuelve() {
    int anterior, actual, nInvariantes = 0, first = true;

    cin >> anterior;
    if (anterior == 0) return false;
    cin >> actual;

    while(anterior) {
        if (anterior+1 == actual) nInvariantes++;
        else {
            if (!first)
                cout << ",";
            if (nInvariantes > 0) {
                cout << anterior-nInvariantes << "-" << anterior;
                nInvariantes = 0;
            } else {
                cout << anterior;
            }
            first = false;
        }

        anterior = actual;
        if (actual > 0) cin >> actual;
    }

    // La siguiente línea debería ser ilegal equisdé
    cout << "\n";

    return true;
}

int main() {
    while(resuelve()) {}
}