#include <iostream>
#include <algorithm>

using namespace std;

// Pre: { 0<=n<=longitud(v) }
int solve(int arr[], int N) {
    int modMax = 1, modCnt = 1, modInd = 0;

    if (N > 1) {
        sort(arr, arr + N); // O(n^2)

        for (int i = 1; i < N; i++) {
            if (arr[i] == arr[i-1]) modCnt++;
            else modCnt = 1;

            if (modCnt > modMax) {
                modMax = modCnt;
                modInd = i;
            }
        }
    }

    return arr[modInd];
}
// Siendo freq(v,n,val): #i : 0<=i<n : v[i] = val
// Post: {freq(v,n,v[ret]) = max i: 0<=i<N : freq(v, n, v[i]) }

int main() {
    int n;
    cin >> n;
    while (n) {
        int * arr = new int [n];
        for (int i = 0; i < n; i++) {
            cin >> arr[i];
        }

        cout << solve(arr, n) << "\n";

        delete [] arr;
        cin >> n;
    }

    return 0;
}