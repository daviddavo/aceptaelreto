#include <iostream>
using namespace std;

int main() {
    int anterior, actual, total;
    cin >> anterior;
    while (anterior >= 0){
        total = 0;
        cin >> actual;
        while (actual >= 0) {
            total += abs(actual-anterior);
            anterior = actual;
            cin >> actual;
        }
        cout << total << "\n";
        cin >> anterior;
    }

    return 0;
}