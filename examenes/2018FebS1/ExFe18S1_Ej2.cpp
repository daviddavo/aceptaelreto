#include <iostream>
using namespace std;

const int MAX_SIZE = 5000;

// Blah blah O(log_2(n))

// Pre: ex k : i<=k<=f : v[k]%2 = 1
int solve(int v[], int i, int f) {
    printf("i: %d, f: %d\n", i, f);
    if (v[i]%2 || i==f) {
        return v[i];
    }

    int m = (i+f)/2;
    if (v[i] + 2*(m-i) != v[m]) {
        // Está en i <-> m
        return solve(v, i, m);
    } else /* if ( v[m+1] + 2*(f-m-1) != v[f]) */ {
        return solve(v, m+1, f);
    }
}
// Pos: r = min k : i<=k<=f : v[k]%2 = 1
// Al ser único, el mínimo y máximo indice son el mismo

inline int solve(int v[], int n) {
    return solve(v, 0, n-1);
}

int main() {
    int n;
    int v[MAX_SIZE];

    scanf("%d", &n);
    while (n) {
        for (int i = 0; i < n; i++)
            scanf("%d", v+i);

        printf("%d\n", solve(v, n));

        scanf("%d", &n);    
    }

    return 0;
}