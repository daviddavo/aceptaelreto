#include <iostream>

using namespace std;

int solve(int ax, int ay, int bx, int by) {
    return (ax-bx)*(ay-by);
}

int main() {
    int ax, ay, bx, by;
    cin >> ax >> ay >> bx >> by;
    while (bx >= ax && by >= ay) {
        cout << solve(ax, ay, bx, by) << "\n";

        cin >> ax >> ay >> bx >> by;
    }

    return 0;
}